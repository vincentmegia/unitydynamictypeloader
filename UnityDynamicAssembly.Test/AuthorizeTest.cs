﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using NUnit.Framework;
using UnityDynamicAssembly.Core;
using UnityDynamicAssembly.Test.Spies;

namespace UnityDynamicAssembly.Test
{
    [TestFixture]
    class AuthorizeTest
    {
        [Test]
        public void AuthorizeActionTest()
        {
            IUnityContainer unityContainer = new UnityContainer();
            var assemblies = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, @"plugins\*.dll")
                .Select(f => Assembly.LoadFile(f)).ToArray();
            unityContainer.RegisterType(typeof(IService), typeof(AuthorizeService));
            unityContainer.RegisterType(typeof(IRepository), typeof(DatabaseRepository));

            var types = from asm in assemblies
                        from t in asm.GetExportedTypes()
                        where typeof(ActionBase).IsAssignableFrom(t) && t.IsClass
                        select t;

            types.ForEach(x =>
            {
                unityContainer.RegisterType(typeof(ActionBase), x, x.Name);
            });

            var authorizeBase = unityContainer.Resolve<ActionBase>("Authorize");
            Assert.IsNotNull(authorizeBase);
        }
    }
}
